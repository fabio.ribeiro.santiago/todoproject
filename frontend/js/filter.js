import RenderTodoItem from "./render.js";

const SEARCH_INPUT = document.getElementById('search_input');
const BTN_FILTER = document.getElementById('btn_filter');

BTN_FILTER.addEventListener('click', () => {
    let searchToken = SEARCH_INPUT.value;
    let url = `${Settings.BASE_API_URL}todoitem/?search=${searchToken}`;
    fetch(url)
    .then(response => response.json())
    .then(results => {
        let items_count = document.getElementById('items_count');
        items_count.innerHTML = `${results.count}`;
        const TABLE_DATA = document.getElementById('table-data');
        TABLE_DATA.innerHTML = '';
        results.results.map(todoitem => {
            TABLE_DATA.innerHTML += RenderTodoItem(todoitem);
        })
    }).catch(function(res){ alert('It was not possible to reach the database. Please try again later!'); });
});
