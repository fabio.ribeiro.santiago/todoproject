function handleCompleteTodoItem(event) {
  let curRow = event.target.closest('tr');

  let url = `${Settings.BASE_API_URL}todoitem/${curRow.dataset.id}/`;
  let data = { "completed": event.target.checked };
  fetch(url,
    {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "PATCH",
      body: JSON.stringify(data)
    })
    .then(function (res) {
      if (event.target.checked) {
        curRow.classList.add('table-success');
        curRow.cells[1].classList.add('task-completed');
      } else {
        curRow.classList.remove('table-success');
        curRow.cells[1].classList.remove('task-completed');
      }

    })
    .catch(function (res) { alert('It was not possible to reach the database. Please try again later!'); })
}


function handleEditTitle(event) {
  event.target.closest('button').disabled = true;
  let curRow = event.target.closest('tr');
  let currentText = curRow.cells[1].innerHTML;
  curRow.cells[1].innerHTML = `<input id="temp_edit_title" type="text" onblur="updateTitle(this, ${curRow.dataset.id}, '${currentText}')" class="form-control" placeholder="${currentText}">`
  document.getElementById('temp_edit_title').focus();
  //curRow.cells[1].setAttribute('contenteditable', true);
  
}

function updateTitle(el, id, oldValue) {
  let curRow = el.closest('tr');

  console.log(el.value);
  console.log(oldValue);

  if ((el.value !== null) && (el.value !== '') && (el.value !== oldValue)) {

    let url = `${Settings.BASE_API_URL}todoitem/${id}/`;
    let data = { "title": el.value };
    fetch(url,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "PATCH",
        body: JSON.stringify(data)
      })
      .then(function (res) {
        console.log('udapted');
      })
      .catch(function (res) { alert('It was not possible to reach the database. Please try again later!'); })

      curRow.cells[1].innerHTML = el.value
  } else {
      curRow.cells[1].innerHTML = oldValue;
  }

  
 curRow.querySelector("button").removeAttribute("disabled");

}


