const Settings = {
    /* Backend base api url */
    BASE_API_URL: 'http://localhost:8000/api/',
    
    /* The value of order field when a new TodoItem is added. It should be a number greater than 10 to reduce the number
    of SQL updates on database */
    INCREMENT_ORDER_FIELD_BY: 20.0
}