function deleteTodoItem(id) {
    let url = `${Settings.BASE_API_URL}todoitem/${id}/`;

     fetch(url, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
    })
    .then(response => {
        document.querySelectorAll(`[data-id='${id}']`).forEach(e => e.remove());
        let items_count = document.getElementById('items_count');
        let count = parseInt(items_count.innerHTML);
        if (count > 0) {
            count = count - 1;
            items_count.innerHTML = count;
        }
    });


}

