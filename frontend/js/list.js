import RenderTodoItem from "./render.js";

const LIST_URL = `${Settings.BASE_API_URL}todoitem/`;

fetch(LIST_URL)
    .then(response => response.json())
    .then(results => {
        let items_count = document.getElementById('items_count');
        items_count.innerHTML = `${results.count}`;
        const TABLE_DATA = document.getElementById('table-data');
        TABLE_DATA.innerHTML = '';
        results.results.map(todoitem => {
            TABLE_DATA.innerHTML += RenderTodoItem(todoitem);
        })
    }).catch(function(res){ alert('It was not possible to reach the database. Please try again later!'); });



