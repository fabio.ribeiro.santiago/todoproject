const RenderTodoItem = (todoitem) => {
    return `
            <tr data-id="${todoitem.id}" data-order="${todoitem.order}" class="${todoitem.completed && 'table-success'}">
                <td><input class="form-check-input" type="checkbox" ${todoitem.completed && 'checked'} onchange="handleCompleteTodoItem(event)"></input></td>
                <td class="${todoitem.completed && 'task-completed'}">${todoitem.title}</td>
                <td class="text-end">
                    <button onclick="handleEditTitle(event)" class="btn btn-warning">
                        <i class="bi bi-pencil-square"></i>
                    </button>
                    <button onclick="deleteTodoItem(${todoitem.id})" class="btn btn-danger">
                        <i class="bi bi-trash-fill"></i>
                    </button>
                </td>
            </tr>
        `
}


export default RenderTodoItem;