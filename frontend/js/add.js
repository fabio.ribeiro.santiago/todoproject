import RenderTodoItem from "./render.js";

const BTN_ADD = document.getElementById('btn_add');

BTN_ADD.addEventListener('click', (event) => {
    
    const tableData = document.getElementById('table-data');
    let order = Settings.INCREMENT_ORDER_FIELD_BY;
    let lastRow = null;

    if (tableData.rows.length > 0) {
        lastRow = tableData.rows[ tableData.rows.length - 1];
        order += parseFloat(lastRow.dataset.order)
    }

    let url = `${Settings.BASE_API_URL}todoitem/`;
    let data = {
        "title": 'New task',
        "completed": false,
        "order": order
    };

    fetch(url,
    {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(todoitem => {
        tableData.insertAdjacentHTML('beforeend', RenderTodoItem(todoitem));
        let items_count = document.getElementById('items_count');
        let count = parseInt(items_count.innerHTML);
        count += 1
        items_count.innerHTML = count;
    })
    .catch(function(response){ alert('It was not possible to add a new task. Please try again later!'); })


})