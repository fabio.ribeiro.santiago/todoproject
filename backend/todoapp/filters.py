import django_filters
from todoapp.models import TodoItem

class TodoItemFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr='stistartswith')

    class Meta:
        model = TodoItem
        fields = ['title']