from django.db import models

class TodoItem(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False)
    order = models.DecimalField(db_index=True, max_digits=19, decimal_places=10)
    completed = models.BooleanField(default=False)
 
    def __str__(self):
        return self.title
