from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from todoapp.serializers import TodoItemSerializer
from todoapp.models import TodoItem
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from django.db import transaction
from rest_framework import status
from rest_framework import filters
from todoapp.filters import TodoItemFilter



class TodoItemViewSet(viewsets.ModelViewSet):

    permission_classes = [AllowAny]
    queryset = TodoItem.objects.all().order_by('order')
    serializer_class = TodoItemSerializer
    filter_backends = [filters.SearchFilter]
    filterset_class = TodoItemFilter
    search_fields = ['^title']
    ordering_fields = ['order']


    '''
    @action(detail=True,methods=['post'],permission_classes=[AllowAny])
    @transaction.atomic
    def switch_order(self, request, pk):
        """ Exchange the order between 2 TodoItems """
        current_todo_item = TodoItem.objects.get(pk=pk)
        current_order = current_todo_item.order
        exchange_with_todo_item = TodoItem.objects.get(pk=request.data['switch_order_with'])
        exchange_with_order = exchange_with_todo_item.order
        current_todo_item.order = exchange_with_order
        exchange_with_todo_item.order = current_order
        current_todo_item.save()
        exchange_with_todo_item.save()
        return Response({'status':'Orders switched'})
    '''
        
        
