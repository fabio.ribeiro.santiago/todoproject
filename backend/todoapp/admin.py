from django.contrib import admin
from todoapp.models import TodoItem

@admin.action(description='Rebuild order - Must select all %(verbose_name_plural)s')
def rebuild_order(self, request, queryset):
    count = 0
    queryset = TodoItem.objects.all().order_by('order')
    for item in queryset:
        count+=1
        item.order = count*20
        item.save()


class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'order')
    actions = [rebuild_order]

admin.site.register(TodoItem, TodoItemAdmin)

